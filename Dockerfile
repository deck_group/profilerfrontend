FROM node:12.18-buster-slim as builder

RUN apt-get update && apt-get -y install yarn git bash

RUN yarn global add ember-cli@3.4.4

WORKDIR /app

COPY . .
RUN yarn install

#RUN ember build --environment production
RUN ember build


FROM klovercloud/node-web-server:12.13.1

WORKDIR /usr/src/app/public
COPY --from=builder /app/dist/. .
ENV PORT 4200
EXPOSE 4200